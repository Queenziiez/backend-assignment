package com.example.demo.repository;

import com.example.demo.model.Employee;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeRepositoryTest {

    @Mock
    private EmployeeRepository employeeRepository;

    private Employee employee1,employee2;

    @BeforeEach
    @Transactional
    void setUp() throws Exception {
        employee1 = new Employee();
        employee1.setEmployee_id(1L);
        employee1.setEmployee_name("Ton");
        employee1.setEmployee_email("Ton@scb.co.th");
        employee1.setEmployee_department("Mae Ma Nee Dev");

        employee2 = new Employee();
        employee2.setEmployee_id(2L);
        employee2.setEmployee_name("Siri");
        employee2.setEmployee_email("Siri@scb.co.th");
        employee2.setEmployee_department("Human Resource");
    }

    @DisplayName("Test findAll repository")
    @Test
    @Rollback(value = false)
    public void findAllTest() throws Exception {
        given(employeeRepository.findAll()).willReturn(List.of(employee1));
        List<Employee> findAll = employeeRepository.findAll();
        assertThat(findAll).isNotNull();
    }

    @DisplayName("Test findById repository")
    @Test
    public void findByIdTest() throws Exception {
        given(employeeRepository.findById(1L)).willReturn(Optional.of(employee1));
        Employee findById = employeeRepository.findById(1L).get();

        assertThat(findById.getEmployee_id()).isEqualTo(1L);
        assertThat(findById.getEmployee_name()).isEqualTo("Ton");
        assertThat(findById.getEmployee_email()).isEqualTo("Ton@scb.co.th");
        assertThat(findById.getEmployee_department()).isEqualTo("Mae Ma Nee Dev");
    }

    @DisplayName("Test save repository")
    @Test
    public void saveEmployeeTest() throws Exception {
        given(employeeRepository.save(employee1)).willReturn(employee1);
        Employee saveEmployee = employeeRepository.save(employee1);

        assertThat(saveEmployee).isNotNull();
    }

    @DisplayName("Test delete repository")
    @Test
    public void deleteEmployeeTest() throws Exception {

//        given(employeeRepository.findById(1L)).willReturn(Optional.of(employee1));

        employeeRepository.deleteById(1L);
        Employee employee = null;
        Optional<Employee> findDeletedEmployee = employeeRepository.findById(1L);

        if (findDeletedEmployee.isPresent()){
            employee = findDeletedEmployee.get();
        }
        assertThat(employee).isNull();
    }

}