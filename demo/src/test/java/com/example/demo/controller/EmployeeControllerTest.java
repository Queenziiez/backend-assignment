package com.example.demo.controller;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WebMvcTest(EmployeeController.class)
@ContextConfiguration(classes={EmployeeController.class})
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    EmployeeService employeeService;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EmployeeRepository employeeRepository;

    private Employee employee1,employee2;

    @BeforeEach
    void setUp() throws Exception {
        employee1 = new Employee();
        employee1.setEmployee_id(1L);
        employee1.setEmployee_name("Ton");
        employee1.setEmployee_email("Ton@scb.co.th");
        employee1.setEmployee_department("Mae Ma Nee Dev");

        employee2 = new Employee();
        employee2.setEmployee_id(2L);
        employee2.setEmployee_name("Siri");
        employee2.setEmployee_email("Siri@scb.co.th");
        employee2.setEmployee_department("Human Resource");
    }

    @DisplayName("Test findAll controller")
    @Test
    void findAllTest() throws Exception {
        List<Employee> employeeList = new ArrayList<>();

        given(employeeService.findAll()).willReturn(employeeList);

        ResultActions response;
        response = mockMvc.perform(get("/api/v1/employees"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @DisplayName("Test findById controller")
    @Test
    void findByIdTest() throws Exception {
        given(employeeService.findById(1L)).willReturn(Optional.of(employee1));

        ResultActions response = mockMvc.perform(get("/api/v1/employees/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employee_name", Matchers.is(employee1.getEmployee_name())))
                .andExpect(jsonPath("$.employee_email", Matchers.is(employee1.getEmployee_email())))
                .andExpect(jsonPath("$.employee_department", Matchers.is(employee1.getEmployee_department())))
                .andDo(print());
    }

    @DisplayName("Test create controller")
    @Test
    void createTest() throws Exception {
        given(employeeService.create(any(Employee.class))).willReturn(employee1);

        ResultActions response = mockMvc.perform(post("/api/v1/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employee1)));

        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.employee_name", Matchers.is(employee1.getEmployee_name())))
                .andExpect(jsonPath("$.employee_email", Matchers.is(employee1.getEmployee_email())))
                .andExpect(jsonPath("$.employee_department", Matchers.is(employee1.getEmployee_department())))
                .andDo(print());
    }

    @DisplayName("Test update by put controller")
    @Test
    void updateByPutTest() throws Exception {
        given(employeeService.findById(1L)).willReturn(Optional.of(employee1));

        given(employeeService.update(any(Employee.class))).willReturn(employee1);

        ResultActions response = mockMvc.perform(put("/api/v1/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employee2)));

        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.employee_name", Matchers.is(employee2.getEmployee_name())))
                .andExpect(jsonPath("$.employee_email", Matchers.is(employee2.getEmployee_email())))
                .andExpect(jsonPath("$.employee_department", Matchers.is(employee2.getEmployee_department())))
                .andDo(print());
    }

    @DisplayName("Test update by patch controller")
    @Test
    void updateByPatchTest() throws Exception {
        given(employeeService.findById(1L)).willReturn(Optional.of(employee1));

        given(employeeService.update(any(Employee.class))).willReturn(employee1);

        Employee employee_for_patch = new Employee();

        employee_for_patch.setEmployee_name("employee_name_for_patch_test");

        ResultActions response = mockMvc.perform(patch("/api/v1/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employee_for_patch)));

        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.employee_name", Matchers.is(employee_for_patch.getEmployee_name())))
                .andExpect(jsonPath("$.employee_email", Matchers.notNullValue()))
                .andExpect(jsonPath("$.employee_department", Matchers.notNullValue()))
                .andDo(print());
    }

    @DisplayName("Test delete controller")
    @Test
    void deleteTest() throws Exception{

        given(employeeService.findById(1L)).willReturn(Optional.of(employee1));

        willDoNothing().given(employeeService).delete(1L);

        ResultActions response = mockMvc.perform(delete("/api/v1/employees/1"))
                .andExpect(status().isOk())
                .andDo(print());
    }
}
