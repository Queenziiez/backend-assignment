package com.example.demo.service;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
    @Mock
    EmployeeRepository employeeRepository;
    @InjectMocks
    EmployeeServiceImpl employeeServiceImpl;

    private Employee employee;
    @BeforeEach
    void setUp() throws Exception{
        employee = new Employee();
        employee.setEmployee_id(1L);
        employee.setEmployee_name("Ton");
        employee.setEmployee_email("ton@scb.co.th");
        employee.setEmployee_department("Mae Ma Nee Dev");
    }

    @DisplayName("Test findAll service")
    @Test
    void findAllTest(){
        given(employeeRepository.findAll()).willReturn(List.of(employee));

        List<Employee> employeeList = employeeServiceImpl.findAll();

        assertThat(employeeList).isNotNull();
    }

    @DisplayName("Test create service")
    @Test
    void createTest(){
        given(employeeRepository.save(employee)).willReturn(employee);

        Employee createdEmployee = employeeServiceImpl.create(employee);

        assertThat(createdEmployee).isNotNull();
    }

    @DisplayName("Test findById service")
    @Test
    void findByIdTest(){
        given(employeeRepository.findById(1L)).willReturn(Optional.of(employee));

        Employee findEmployee = employeeServiceImpl.findById(employee.getEmployee_id()).get();

        assertThat(findEmployee).isNotNull();
        assertThat(findEmployee.getEmployee_id()).isEqualTo(employee.getEmployee_id());
    }

    @DisplayName("Test update service")
    @Test
    void updateTest(){
        given(employeeRepository.save(employee)).willReturn(employee);

        employee.setEmployee_name(updateData.employee_name);
        employee.setEmployee_email(updateData.employee_email);
        employee.setEmployee_department(updateData.employee_department);

        Employee updatedEmployee = employeeServiceImpl.update(employee);

        assertThat(updatedEmployee).isNotNull();
        assertThat(updatedEmployee.getEmployee_name()).isEqualTo(updateData.employee_name);
        assertThat(updatedEmployee.getEmployee_email()).isEqualTo(updateData.employee_email);
        assertThat(updatedEmployee.getEmployee_department()).isEqualTo(updateData.employee_department);
    }

    @DisplayName("Test delete service")
    @Test
    void deleteTest(){
        willDoNothing().given(employeeRepository).deleteById(1L);

        employeeServiceImpl.delete(1L);

        verify(employeeRepository,times(1)).deleteById(1L);
    }

    interface updateData {
        String employee_name = "newTonName";

        String employee_email = "newTonEmail@scb.co.th";

        String employee_department = "newTonDepartment";
    }

}
