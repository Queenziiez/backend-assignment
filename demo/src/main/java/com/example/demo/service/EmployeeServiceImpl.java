package com.example.demo.service;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.flywaydb.core.internal.jdbc.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> findAll(){
        return repository.findAll();
    }


    @Override
    public Optional<Employee> findById(long id){
        return repository.findById(id);
    }

    @Override
    public Employee create(Employee employee){
        return repository.save(employee);
    }

    @Override
    public Employee update(Employee employee){
        return repository.save(employee);
    }

    @Override
    public void delete(long id){
        repository.deleteById(id);
    }
}
