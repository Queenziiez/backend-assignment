package com.example.demo.batch;

import com.example.demo.model.Employee;
import org.jetbrains.annotations.NotNull;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.HashMap;


@Component
public class Processor implements ItemProcessor<Employee, Employee> {


    private static final Map<String, String> DEPT_NAMES = new HashMap<>();

    public Processor() {
        DEPT_NAMES.put("10","IT");
        DEPT_NAMES.put("20","HR");
        DEPT_NAMES.put("30","QA");
    }


    @Override
    public Employee process(Employee employee) throws Exception {

        String deptCode = employee.getEmployee_department();
        String dept = DEPT_NAMES.get(deptCode);
        employee.setEmployee_department(dept);
        System.out.println(String.format("Converted from "+deptCode+" to "+dept));
        return employee;
    }
}
