package com.example.demo.model;

import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "EMPLOYEE")
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="employee_id")
    private long employee_id;

    @Column(name ="employee_name")@NotNull(message = "Please enter employee_name.")
    private String employee_name;

    @Column(name ="employee_email")@Email
    private String employee_email;

    @Column(name ="employee_department")
    private String employee_department;

    public Employee() {
    }

    public Employee(String employee_name, String employee_email, String employee_department) {
        this.employee_name = employee_name;
        this.employee_email = employee_email;
        this.employee_department = employee_department;
    }
}
