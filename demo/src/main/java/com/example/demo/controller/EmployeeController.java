package com.example.demo.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Employee;

import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/api/v1")
public class EmployeeController {

    @Autowired
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    // get employees
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> findAll() {
        List<Employee> list = this.employeeService.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    // get employee by id
    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value= "id") Long employeeId)
        throws ResourceNotFoundException {
        Employee employee = employeeService.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: "+employeeId));
        return ResponseEntity.ok().body(employee);
    }

    // create employee
    @PostMapping("/employees")
    public ResponseEntity<Employee> createEmployee(@RequestBody @Valid Employee employee){
        this.employeeService.create(employee);
        return ResponseEntity.ok().body(employee);
    }

    // update employee
    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updatePutEmployee(@PathVariable(value = "id") Long employeeId,
                                                   @Validated @RequestBody Employee employeeDetails) throws ResourceNotFoundException {
        Employee employee = employeeService.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: "+employeeId));
        employee.setEmployee_email(employeeDetails.getEmployee_email());
        employee.setEmployee_name(employeeDetails.getEmployee_name());
        employee.setEmployee_department(employeeDetails.getEmployee_department());

        this.employeeService.update(employee);

        return ResponseEntity.ok().body(employee);
    }

    // update employee (patch)
    @PatchMapping("/employees/{id}")
    public ResponseEntity<Employee> updatePatchEmployee(@PathVariable(value = "id") Long employeeId,
                                                   @Valid @RequestBody Employee employeeDetails) throws ResourceNotFoundException {
        Employee employee = employeeService.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: "+employeeId));

        if (employeeDetails.getEmployee_email()!=null) employee.setEmployee_email(employeeDetails.getEmployee_email());
        if (employeeDetails.getEmployee_name()!=null) employee.setEmployee_name(employeeDetails.getEmployee_name());
        if (employeeDetails.getEmployee_department()!=null) employee.setEmployee_department(employeeDetails.getEmployee_department());

        this.employeeService.update(employee);

        return ResponseEntity.ok().body(employee);
    }
//
    // delete employee
    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId) throws ResourceNotFoundException {
        Employee employee = employeeService.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: "+employeeId));

        this.employeeService.delete(employeeId);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }
}
