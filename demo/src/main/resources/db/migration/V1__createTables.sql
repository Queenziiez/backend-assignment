CREATE TABLE EMPLOYEE (
    employee_id serial NOT NULL,
    employee_name char(255) NOT NULL,
    employee_email char(255) NOT NULL,
    employee_department char(255) NOT NULL,
    PRIMARY KEY(employee_id)
);